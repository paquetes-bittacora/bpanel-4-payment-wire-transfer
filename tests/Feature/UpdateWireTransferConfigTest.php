<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Tests\Feature;

use Bittacora\Bpanel4\Payment\WireTransfer\Actions\UpdateBankWireConfiguration;
use Bittacora\Bpanel4\Payment\WireTransfer\Dtos\WireTransferConfigDto;
use Bittacora\Bpanel4\Payment\WireTransfer\Models\WireTransferConfig;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class UpdateWireTransferConfigTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        WireTransferConfig::create();
    }

    public function testActualizaLaConfiguracionDelPagoPorTransferenciaBancaria(): void
    {
        $action = $this->app->make(UpdateBankWireConfiguration::class);

        $instructions = 'instrucciones del pago por transferencia ' . time();
        $action->handle(new WireTransferConfigDto($instructions));

        self::assertEquals($instructions, WireTransferConfig::first()->user_instructions);
    }
}
