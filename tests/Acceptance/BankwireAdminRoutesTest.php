<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Tests\Acceptance;

use Bittacora\Bpanel4\Payment\WireTransfer\Models\WireTransferConfig;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class BankwireAdminRoutesTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        WireTransferConfig::create();
        $this->actingAs((new UserFactory())->withPermissions('bpanel4-bankwire-payment.index', 'bpanel4-bankwire-payment.update')
            ->createOne());
    }

    public function testMuestraLaVistaDeEdicionDePagoPorTransferencia(): void
    {
        $response = $this->get(route('bpanel4-bankwire-payment.index'));
        $response->assertOk();
        $response->assertSee('Pago por transferencia');
    }

    public function testSePuedeGuardarLaConfiguracionDelPagoPorTransferencia(): void
    {
        $instructions = $this->faker->text();
        $response = $this->followingRedirects()->put(route('bpanel4-bankwire-payment.update'), ['instructions' => $instructions]);
        $response->assertOk();
        $response->assertSee('Configuración actualizada');
    }
}
