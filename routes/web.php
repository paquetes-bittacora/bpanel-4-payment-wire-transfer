<?php

declare(strict_types=1);


use Bittacora\Bpanel4\Payment\WireTransfer\Http\Controllers\BankwirePaymentAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/pago-por-transferencia')->name('bpanel4-bankwire-payment.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/configurar', [BankwirePaymentAdminController::class, 'index'])->name('index');
        Route::put('/guardar-configuracion', [BankwirePaymentAdminController::class, 'update'])->name('update');
    });
