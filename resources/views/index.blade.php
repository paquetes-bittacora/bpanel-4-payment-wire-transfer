@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-bankwire-payment::general.index'))

@section('content')
    @include('bpanel4-bankwire-payment::_form')
@endsection
