<div class="card bcard">
    <div class="card-header bgc-primary-d1 text-white border-0">
        <h4 class="text-120 mb-0">
            <span class="text-90">{{ __('bpanel4-bankwire-payment::general.index') }}</span>
        </h4>
    </div>
    <form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
        @csrf
        @method('put')
        @livewire('form::textarea', ['name' => 'instructions', 'labelText' =>
        __('bpanel4-bankwire-payment::general.instructions'), 'value' => old('instructions') ??
        $paymentMethod->user_instructions ])
        <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
            @livewire('form::save-button',['theme'=>'update'])
            @livewire('form::save-button',['theme'=>'reset'])
        </div>
    </form>
</div>
