<?php

declare(strict_types=1);

return [
    'index' => 'Opciones de pago por transferencia',
    'instructions' => 'Instrucciones para los clientes',
];
