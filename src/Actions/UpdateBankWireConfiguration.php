<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Actions;

use Bittacora\Bpanel4\Payment\WireTransfer\Dtos\WireTransferConfigDto;
use Bittacora\Bpanel4\Payment\WireTransfer\Models\WireTransferConfig;

final class UpdateBankWireConfiguration
{
    public function handle(WireTransferConfigDto $dto): void
    {
        $config = WireTransferConfig::firstOrFail();
        $config->user_instructions = $dto->instructions;
        $config->save();
    }
}
