<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer;

use Bittacora\Bpanel4\Payment\WireTransfer\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class WireTransferServiceProvider extends ServiceProvider
{
    public const PACKAGE_PREFIX = 'bpanel4-bankwire-payment';

    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang/', self::PACKAGE_PREFIX);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->commands(InstallCommand::class);
    }
}
