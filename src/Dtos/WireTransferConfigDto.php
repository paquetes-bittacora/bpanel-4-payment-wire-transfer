<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Dtos;

final class WireTransferConfigDto
{
    public function __construct(public readonly string $instructions = '')
    {
    }
}
