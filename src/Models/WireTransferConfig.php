<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $user_instructions
 * @method static self firstOrFail()
 */
final class WireTransferConfig extends Model
{
    /**
     * @var string
     */
    protected $table = 'payment_methods_bankwire_config';
}
