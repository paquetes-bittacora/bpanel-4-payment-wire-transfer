<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class UpdateBankWireConfigRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'instructions' => 'nullable|string',
        ];
    }
}
