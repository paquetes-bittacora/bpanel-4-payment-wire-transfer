<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Payment\WireTransfer\Actions\UpdateBankWireConfiguration;
use Bittacora\Bpanel4\Payment\WireTransfer\Dtos\WireTransferConfigDto;
use Bittacora\Bpanel4\Payment\WireTransfer\Http\Requests\UpdateBankWireConfigRequest;
use Bittacora\Bpanel4\Payment\WireTransfer\Models\WireTransferConfig;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;

final class BankwirePaymentAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $router,
        private readonly Redirector $redirector,
    ) {
    }

    public function index(): View
    {
        $this->authorize('bpanel4-bankwire-payment.index');
        return $this->view->make('bpanel4-bankwire-payment::index', [
            'action' => $this->router->route('bpanel4-bankwire-payment.update'),
            'paymentMethod' => WireTransferConfig::firstOrFail(),
        ]);
    }

    public function update(
        UpdateBankWireConfigRequest $request,
        UpdateBankWireConfiguration $updateBankWireConfiguration
    ): RedirectResponse {
        $this->authorize('bpanel4-bankwire-payment.update');
        try {
            $updateBankWireConfiguration->handle(new WireTransferConfigDto($request->validated('instructions')));
            return $this->redirector->route('bpanel4-bankwire-payment.index')
                ->with(['alert-success' => 'Configuración actualizada']);
        } catch (Exception) {
            return $this->redirector->route('bpanel4-bankwire-payment.index')
                ->with(['alert-danger' => 'Ocurrió un error al actualizar la configuración']);
        }
    }
}
