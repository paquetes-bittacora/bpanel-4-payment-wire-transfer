<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\WireTransfer\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRolePermission;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRow;
use Bittacora\Bpanel4\Payment\Payment;
use Bittacora\Bpanel4\Payment\WireTransfer\Models\WireTransferConfig;
use Bittacora\Bpanel4\Payment\WireTransfer\PaymentMethods\WireTransfer;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    private const PERMISSIONS = ['index', 'update'];

    /**
     * @var string
     */
    protected $signature = 'bpanel4-wire-transfer:install';
    /**
     * @var string
     */
    protected $description = 'Registra las opciones de pago por transferencia en el menú.';

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    public function handle(AdminMenu $adminMenu): void
    {
        $this->addMenuEntries($adminMenu);
        $this->giveAdminPermissions();
        $this->createDefaultConfiguration();
        $this->registerPaymentMethod();
        $this->enableForAllRolesByDefault();
    }

    private function addMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createAction(
            'bpanel4-payment',
            'Pago por transferencia bancaria',
            'index',
            'far fa-university',
            'bpanel4-bankwire-payment.index',
        );
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-bankwire-payment.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createDefaultConfiguration(): void
    {
        WireTransferConfig::create();
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    private function registerPaymentMethod(): void
    {
        /** @var Payment $paymentModule */
        $paymentModule = resolve(Payment::class);
        /** @var WireTransfer $paymentMethod */
        $paymentMethod = resolve(WireTransfer::class);
        $paymentModule->registerPaymentMethod($paymentMethod);
    }

    private function enableForAllRolesByDefault(): void
    {
        $roles = Role::get()->all();
        $paymentMethod = PaymentMethodRow::where('payment_method', WireTransfer::class)->firstOrFail();
        foreach ($roles as $role) {
            PaymentMethodRolePermission::updateOrCreate(
                [
                    'role_id' => $role->id,
                    'payment_method_id' => $paymentMethod->id,
                ],
                ['enabled' => 1]);
        }
    }
}
